console.log("Hello World");

/* window.TrelloPowerUp.initialize({
    'card-badges': function (t, opts) {
        return t.card("all")
        .then(function (card) {
            console.log(card);
            return[{
                text: "hahaha" // Set badge text
            }];
        })
    }
  }); */

  window.TrelloPowerUp.initialize({
    'list-actions': function (t) {
      return t.list('name', 'id')
      .then(function (list) {
        return [{
          text: "Get List Stats",
          callback: function (t) {
            // Trello will call this if the user clicks on this action
            // we could for example open a new popover...
            t.popup({
              // ...
            });
          }
        }];
      });
    }
  });